from django import forms
from .models import StatusKu

class StatusForm(forms.Form):
    title = forms.CharField(label = "Status", max_length = 30, widget = forms.TextInput(
        attrs = {'required':True,}
    ))
    