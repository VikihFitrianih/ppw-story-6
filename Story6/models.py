from django.db import models
from django.utils import timezone

# Create your models here.
class StatusKu(models.Model):
    title = models.CharField(max_length=300)
    # date = models.DateTimeField(auto_now_add = True, null = True)
    time = models.TimeField(default=timezone.localtime(timezone.now()))
    date = models.DateField(default=timezone.now)

    def __str__(self):
        return self.title