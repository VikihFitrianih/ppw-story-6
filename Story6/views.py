from django.shortcuts import render, redirect
from .models import StatusKu
from .forms import StatusForm

def buat_status(request):
    if request.method == "POST":
        form = StatusForm(request.POST or None)
        if form.is_valid():
            status = StatusKu()
            status.title = form.cleaned_data['title']
            status.save()
        return redirect('/')
    else:
        form = StatusForm()
        status = StatusKu.objects.all()
        response = {'status': status, 'title': 'List', 'form':form}
        return render(request, "home.html", response)